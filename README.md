# FixxIT Angular Challenge

### Welcome fellow coder!
![Fellow Coder](https://media.giphy.com/media/OIEhvGRByVrHO/giphy.gif)

#### Introduction
---
You have chosen to undertake the FixxIT Angular Challenge. This is a challenge and not a test! It is not about passing or failing, but rather about us giving you a clean canvas that you can use to demonstrate how passionate you are about coding. Thus, go forth **champion** and prove to us that you have what it takes to survive the every day life as a code wielding bug slayer!

Are you ready? 

![Yes Sir](https://media.giphy.com/media/RavXJWRY3veEw/giphy.gif)

If that is your response, or something similar, we should probably get down to the details!

#### Before you start
---

As you might have noticed already, we have provided you with a "seed project" to get things going. This should give you a good point to start from without you having to setup things from scratch. *A lazy developer is a good developer.*

A few things that you should know before you start:
- Please read through the entire challenge before starting.
- You have only one week to finish the challenge.
- Please do frequent and well commented commits.
- Commenting your code is not mandatory, but will be appreciated.
- It does not have to be the prettiest.
- You are welcome to use a different Angular 6 seed project, but please provide the original link.
- Not all phases listed below are mandatory.
- It is not a race to try and finish all phases.
- Please tag the final commit of each phase completed.

#### Challenge description:
---

###### Background
So, I have this buddy called Frikkie. He who owns a second hand car garage that is called...wait for it...**Frikkie's cabs**, original right?

He would like you to create a website for his shop. These days Frikkie only sells his second hand cars online and could use a website of his own. With that said I guess we should do this in phases, because you know...agile and what not. So best you start at **phase 1** then!

###### Phase 1:
Lets start by creating a page that will display all of the cars that Frikkie has across all of his warehouses. Please sort the list according to *date_added* asc. In the project we added a file called *db.json* that contains all the second hand cars the Frikkie has available for sale.  

###### Phase 2:
Now, allowing the user to click on any of the cars (that are licensed(true)) would make it a lot nicer. Once the user clicked on a car we could then also show a details page that displays things such as, the warehouse where it is stored and its location perhaps? It is up to you.

###### Phase 3:
Wow! Okay, we now have some good phunctionality! So lets take it one step further! Lets allow the user to add the car he is viewing to some sort of *shopping cart* so that he can easily checkout once he is done shopping. Oh, and maybe give him the total amount as well.   

###### Phase 4:
Frikkie is also struggling with his admin. Can you maybe come up with an easy way that he can also see a list of the cars that are not licensed(false)?


#### Side notes:
---
- Kudos if you leave the project in working order.
- Kudos if you have unit test coverage.
- Kudos if you use es6.
- Kudos if you have no linting issues.
- Kudos if you use Redux to manage your shopping cart.

#### Getting started:
---
To get started, please fork this repository and create a branch with the same name as the key that was given to you via email. If you have not received a key yet, please contact <maruschka@fixx.it> or me <francois.vandermerwe@fixx.it>. Once you have finished the challenge or you have ran out of time, please put in a merge request to the develop branch of this repository.

After we had time to review your awesome solution we will get back to you.

If there is something that you do not know then we suggest that you take the opportunity and
 
![Google IT](https://media.giphy.com/media/XPmVwGTPC149a/giphy.gif)


##### Good luck and have fun!

# Project Table of Contents

- [How to start](#how-to-start)
- [Running tests](#running-tests)

# How to start

In order to start the project after cloning:

```bash
# install the project's dependencies
$ npm install
# fast install (via Yarn, https://yarnpkg.com)
$ yarn install  # or yarn

# watches your files and uses livereload by default
$ npm start

# to start deving with livereload site and coverage as well as continuous testing
$ npm run start.deving

# dev build
$ npm run build.dev
# prod build, will output the production application in `dist/prod`
# the produced code can be deployed (rsynced) to a remote server
$ npm run build.prod
# prod build using different base path
$ npm run build.prod -- --base "/foo/bar/"
```

# Running tests

```bash
$ npm test

# Development. Your app will be watched by karma
# on each change all your specs will be executed.
$ npm run test.watch
# NB: The command above might fail with a "EMFILE: too many open files" error.
# Some OS have a small limit of opened file descriptors (256) by default
# and will result in the EMFILE error.
# You can raise the maximum of file descriptors by running the command below:
$ ulimit -n 10480


# code coverage (istanbul)
# auto-generated at the end of `npm test`
# view coverage report:
$ npm run serve.coverage

# e2e (aka. end-to-end, integration)  - In two different shell windows
$ npm start
$ npm run e2e

# e2e - In one shell window (especially useful for Continuous Integration)
$ npm run e2e.ci

# e2e live mode - Using Cypress app - In two different shell windows
$ npm start
$ npm run e2e.live
```
